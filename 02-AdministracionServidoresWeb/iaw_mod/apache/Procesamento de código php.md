﻿


> Written with [StackEdit](https://stackedit.io/).
> ## Apache2 y módulo PHP

Hemos instalado apache2 y el módulo que permite que los procesos de apache2 sean capaz de ejecutar el código PHP, 

```
apt install apache2 php7.2 libapache2-mod-php7.2

```
pero que hace que el rendimiento de apache sea menor ya que con esta instalación se desactiva el MPM  `event`  y se activa el  `prefork`:

```
...
Module mpm_event disabled.
Enabling module mpm_prefork.
apache2_switch_mpm Switch to prefork
...

```
Si vemos el contenido del fichero de configuración del módulo php de apache2,  `/etc/apache2/mods-available/php7.2.conf`, nos encontramos las siguientes líneas:

```
<FilesMatch ".+\.ph(p[3457]?|t|tml)$">
    SetHandler application/x-httpd-php
</FilesMatch>
...

```
Donde se crea un nuevo manejador, que hace que los ficheros cuya extensión es  `php`  sean gestionados por el módulo que interpreta el código php.

### Configuración de php 

La configuración de php está dividida en distintos directorios para las distintas formas de ejecutar el código php:

-   `/etc/php/7.2/cli`: Configuración de php para  `php7.2-cli`, cuando se utiliza php desde la línea de comandos.
-   `/etc/php/7.2/apache2`: Configuración de php para apache2 cuando utiliza el módulo php.
-   `/etc/php/7.2/fpm`: Configuración de php para php-fpm.
-   `/etc/php/7.2/mods-available`: Módulos disponibles de php que puedes estar configurados en cualquiera de los escenarios anteriores.

Si nos fijamos en la configuración de php para apache2:

-   `/etc/php/7.2/apache2/conf.d`: Módulos instalados en esta configuración de php (enlaces simbólicos a  `/etc/php/7.2/mods-available`).
-   `/etc/php/7.2/apache2/php.ini`: Configuración de php para este escenario.

## PHP-FPM

FPM (FastCGI Process Manager) se encarga de interpretar código PHP. Normalmente se utiliza junto a un servidor web (Apache2 o ngnix)

Para instalarlo en Debian 9:

```
apt install php7.2-fpm php7.2

``` 
Ahora podemos desactivar el módulo de php de apache y volver a activar el MPM event.

```
a2dismod php7.2
a2dismod mpm_prefork
a2enmod mpm_event

```
Se crean entonces unos procesos php-fpm que se quedan escuchando por un socket UNIX o TCP. 
Ahora si el servidor apache recibe peticiones de recursos php ya no las ejecutan los propios procesos de apache con el módulo de php de apache (que acabamos de deshabilitar), sino que actuando como **proxy inverso**   las envían al socket UNIX/TCP donde son tratadas por los procesos php-fpm.
Para ver los procesos de apache en modo event :

    ps -A | grep apache2
Para ver los procesos de php-fpm:

    ps -A | grep fpm

 Apache envía las peticiones php a estos procesos, que ejecutan el código php y devuelven el resultado al servidor y éste envía al cliente la respuesta con el HTML generado.
 
### Configuración php con fpm

Ficheros de configuración de php para php-fpm:

-   `/etc/php/7.2/fpm/conf.d`: Módulos instalados en esta configuración de php (enlaces simbólicos a  `/etc/php/7.2/mods-available`).
-   `/etc/php/7.2/fpm/php-fpm.conf`: Configuración general de php-fpm.
-   `/etc/php/7.2/fpm/php.ini`: Configuración de php para este escenario.
-   `/etc/php/7.2/fpm/pool.d`: Directorio con distintos pool de configuración. Cada aplicación puede tener una configuración distinta (procesos distintos) de php-fpm.

Por defecto tenemos un pool cuya configuración la encontramos en  `/etc/php/7.2/fpm/pool.d/www.conf`, en este fichero podemos configurar muchos parámetros, los más importantes son:

-   `[www]`: Es el nombre del pool, si tenemos varios, cada uno tiene que tener un nombre.
-   `user`  y  `grorup`: Usuario y grupo con el que se va ejecutar los procesos.
-   `listen`: Se indica el socket unix o el socket TCP donde van a escuchar los procesos:
    
    -   Por defecto, escucha por un socket unix:  
        `listen = /run/php/php7.2-fpm.sock`
    -   Si queremos que escuche por un socket TCP:  
        `listen = 127.0.0.1:9000`
    -   En el caso en que queramos que escuche en cualquier dirección:  
        `listen = 9000`

Si hacemos cambios, reiniciamos el servicio:

```
systemctl restart php7.2-fpm

```

### Configuración de Apache2 con php-fpm

Se trata de configurar el servidor apache (para uno o para todos los sitios web ) como un proxy inverso para que las peticiones de recursos php las envíe a los procesos php-fm.

Necesito activar los siguientes módulos:
```
a2enmod proxy proxy_fcgi

```
y reiniciar el servidor apache.

#### Activarlo para cada virtualhost

 Teniendo en cuenta que por defecto php-fpm está escuchando por un socket podemos hacerlo de 2 maneras desde el fichero de configuración del virtual host: 
 
-   Si php-fpm está escuchando en un socket UNIX (opción por defecto, válida para la comunicación entre procesos dentro de un mismo sistema unix):
    
    ```
      ProxyPassMatch ^/(.*\.php)$ unix:/run/php/php7.2-fpm.sock|fcgi://127.0.0.1/var/www/html
    
  en este caso para  el sitio por defecto.
    
-  Si instalamos apache y php en equipos diferentes, entonces  php-fpm tendría que estar escuchando en un socket TCP (hay que pasarle dirección IP del equipo y el puerto donde estaría escuchando php)
    
    ```
      ProxyPassMatch ^/(.*\.php)$ fcgi://127.0.0.1:9000/var/www/html/$1
    
también para  el sitio por defecto.  ```
  
Otra forma de hacerlo es la siguiente:

-   Si php-fpm está escuchando en un socket TCP:
    
    ```
      <FilesMatch "\.php$">
          SetHandler "proxy:fcgi://127.0.0.1:9000"
      </FilesMatch>
    
    ```
    
-   Si php-fpm está escuchando en un socket UNIX:
    
    ```
      <FilesMatch "\.php$">
             SetHandler "proxy:unix:/run/php/php7.2-fpm.sock|fcgi://127.0.0.1/"
      </FilesMatch>
    
    ```    
Después de salvar los cambios habría que reiniciar apache

#### Activarlo para todos los virtualhost

Tenemos a nuestra disposición un fichero de configuración  `php7.2-fpm`  en el directorio  `/etc/apache2/conf-available`. Por defecto funciona cuando php-fpm está escuchando en un socket UNIX, si escucha por un socket TCP, hay que cambiar la línea:

```
SetHandler "proxy:unix:/run/php/php7.2-fpm.sock|fcgi://localhost"

```

por esta:

```
SetHandler "proxy:fcgi://127.0.0.1:9000"

```

Por último activamos la configuración:

```
a2enconf php7.2-fpm
```

