﻿


> Written with [StackEdit](https://stackedit.io/).

# Opciones para directorios y ficheros

## Directory

En  [`Directory`](http://httpd.apache.org/docs/2.4/mod/core.html#directory), podemos indicar algunas opciones para directorios del servidor mediante la directiva  [`Options`](http://httpd.apache.org/docs/2.4/mod/core.html#options). Algunas de las opciones que podemos indicar son las siguientes:

-   `All`: Todas las opciones excepto  `MultiViews`.
-   `FollowSymLinks`: Se pueden seguir los enlaces simbólicos.
-   `Indexes`: Cuando accedemos al directorio y no se encuentra un fichero por defecto (indicado en la directiva  `DirectoryIndex`  del módulo  `mod_dir`), por ejemplo el  `index.html`, se muestra la lista de ficheros (esto lo realiza el módulo  `mod_autoindex`).
-   `MultiViews`: Permite la  [negociación de contenido](http://httpd.apache.org/docs/2.4/content-negotiation.html), mediante el módulo  `mod_negotiation`.
-   `SymLinksIfOwnerMatch`: Se pueden seguir enlaces simbólicos, sólo cuando el fichero destino es del mismo propietario que el enlace simbólico.
-   `ExecCGI`: Permite ejecutar script CGI usando el módulo  `mod_cgi`.

Podemos activar o desactivar una opción en referencia con la configuración de un directorio padre mediante el signo  `+`  o  `-`.

### Ejemplos

En el fichero  `/etc/apache2/apache2.conf`, nos encontramos el siguiente código:

```
<Directory /var/www/>
    Options Indexes FollowSymLinks
    ...

```

A continuación podría cambiar las opción del virtual host  `pagina1`, incluyendo en su fichero de configuración:

```
<Directory /var/www/pagina1>
    Options -Indexes +Multiviews
    ...
```
En este caso para pagina1 y sus subdirectorios quito la opción Indexes y añado Multiviews respecto a la configuración heredada del directorio */var/www*

```
<Directory /var/www/pagina1>
    Options Indexes
    ...
```
En este caso para pagina1 y sus subdirectorios quito todas las opciones heredadas del directorio */var/www* y establezco la opción Indexes

## Alias

Las directiva  [`Alias`](http://httpd.apache.org/docs/2.4/mod/mod_alias.html#alias)  nos permite que cuando accedemos a una url el servidor sirva ficheros desde cualquier ubicación aunque no estén en el  `DocumentRoot`.

Por ejemplo si pongo este alias en el fichero de configuración de  `pagina1`:

```
Alias "/image" "/ftp/pub/image"

```

Puedo acceder, por ejemplo, a una imagen con la URL  `www.pagina1.org/image/logo.jpg`.

No basta con poner la directiva  `Alias`, además es necesario dar permiso de acceso al directorio, por lo tanto tendremos que poner:

```
Alias "/image" "/ftp/pub/image"
<Directory "/ftp/pub/image">
    Require all granted
</Directory>

```

## Redirecciones

> La directiva  [`redirect`](https://httpd.apache.org/docs/2.4/mod/mod_alias.html#redirect)  es usada para pedir al cliente que haga otra petición a una URL o recurso diferente. 
> El cliente hace una primera petición a un recurso que ya no existe, normalmente porque el recurso al que queremos acceder ha cambiado de localización.
> El servidor responde al cliente informándole que tiene que hacer una nueva petición y enviándole la dirección con la nueva ubicación del recurso solicitado.
> En el caso de usar un Alias sólo se hace una petición.

Podemos crear redirecciones de dos tipos:

-   **Permanentes**: se da cuando el recurso sobre el que se hace la petición ha sido ‘movido permanentemente‘ hacia una dirección distinta, es decir, hacia otra URL. Se devuelve el código de estado 301. Es la que debemos realizar cuando queremos cambiar la URL de un recurso para que los buscadores, **por ejemplo cuando cambiamos de dominio**, sigan guardando la posición que tenía nuestra página.
-   **Temporales**: se da cuando el recurso sobre el que se hace la petición ha sido “encontrado” pero reside temporalmente en una dirección distinta, es decir, en otra URL. Se devuelve un código de estado 302. Es la opción por defecto.

### Ejemplos de redirecciones temporales

```
Redirect "/service" "http://www.pagina.com/service"
Redirect "/one" "/two"

```

Como vemos podemos hacer redirecciones a recursos de otro sitio u otro servidor o bien a recursos en el mismo sitio

### Ejemplos de redirecciones permanentes
A un enlace externo e interno respectivamente:
``` 
Redirect permanent "/one" "http://www.pagina2.com/two"
Redirect 301 "/otro" "/otra"

```
