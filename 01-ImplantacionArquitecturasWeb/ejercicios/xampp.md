﻿


> Written with [StackEdit](https://stackedit.io/).
>
 1. apachefriends.org
 3. Qué paquetes de descarga se encuentran y para qué sistemas operativos?
 4. Instalacion Windows Siguiente -> siguiente
 5. Observa qué componentes del paquete de xampp puedes instalar. Indica cuáles hay que seleccionar para instalar un servidor web, un intérprete de php y una base de datos. Selecciona además phpmyadmin.
 6. phpmyadmin es una aplicación web? Explica para qué sirve.
 7. Observa que al finalizar la instalación arranca el panel de control de Xampp. Arranca en el XAMPP Control Panel el servidor web y la base de datos. 
 8. Cómo compruebas que el servidor web ha arrancado correctamente?
 9.  Cuál es la ubicación por defecto de las páginas del servidor web? Y la de los ficheros de configuración de apache?
 10.  En qué ubicación se encuentran las bases de datos de mysql? Crea una nueva base de datos desde phpmyadmin, comprueba que se ha creado en la ubicación anterior.
 

