﻿


> Written with [StackEdit](https://stackedit.io/).
> # El servicio de transferencia de archivos en el proceso de despliegue de una aplicación Web.
> Suele ser típico que cualquier aplicación  web  en Internet disponga de la posibilidad de subir archivos mediante una configuración del código fuente de la misma, una aplicación propia o una aplicación de terceros, como los paneles de administración  web.

Si empleas una aplicación  web  para subir archivos debes tener en cuenta cuánto tiempo puedes mantener la conexión abierta con el servicio  web  y cuál es el tamaño máximo de subida de un archivo. Estas cuestiones suelen ser típicas de la configuración del servidor  web. Por la contra, si empleas un servidor  ftp  dependerá de éste las cuestiones anteriores.

Se suele configurar el servidor  web  con unos parámetros: tiempo de conexión y tamaño máximo de subidas de archivos diferentes del servidor  ftp, de tal forma que para archivos de tamaño no muy grandes se puedan emplear aplicaciones  web  y no se sufra un corte en la subida de archivos y, para archivos grandes, se emplee el servidor  ftp.

Normalmente las empresas de alojamiento  web  (hosting) permiten la subida de archivos mediante un servidor  ftp  y poseen documentos sobre cómo operar con éste, esto es, documentación que explica cómo conectarse a sus servidores  ftp  a través de algún cliente  ftp, como por ejemplo:  FileZilla,  Cute FTP,  Fetch  o  Transmit. También suelen permitir usar  SCP  o  SFTP  para transferir ficheros de forma segura mediante un canal cifrado. Por ejemplo en  Filezilla  se puede establecer la conexión de forma cifrada directamente, sólo con indicar como puerto  TCP  el número del servidor  SSH, por defecto, 22.

También debes saber que muchos editores web permiten subir tu aplicación  web  al servidor con el protocolo  FTP, esto te puede resultar más sencillo que el uso de una aplicación de  FTP  independiente.

Eso si, sea cual sea el método  ftp  que utilices para subir archivos y actualizar tu  web, se desaconseja el uso de aplicaciones no actualizadas que podrían comprometer la seguridad de tu  web.
