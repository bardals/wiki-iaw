﻿DESA (Ubuntu/Debian con  Desktop y browser)
sudo apt-get install python3-venv
python3 -m venv desa
cd desa
source bin/activate
(desa) ubuntu@ip-172-31-95-237:~/desa$ git clone  https://github.com/jbardals/django-tutorial
cd django-tutorial  (aquí repo local de git con el codigo de django)
cat requirements.txt
3. (desa) ubuntu@ip-172-31-95-237:~/desa/django-tutorial$ pip3 install -r requirements.txt
4. settings.py
5. (desa) ubuntu@ip-172-31-95-237:~/desa/django-tutorial$ python3 manage.py migrate
6. python3 manage.py createsuperuser
7. python3 manage.py runserver 0.0.0.0:8000
curl localhost:8000/admin
Invalid HTTP_HOST header: '0.0.0.0:8000'. You may need to add '0.0.0.0' to ALLOWED_HOSTS.
Bad Request: /
[20/Feb/2021 12:01:32] "GET / HTTP/1.1" 400 61867
settings.py -> ALLOWED_HOSTS = ['*']

SFTP
GIT
ubuntu@ip-172-31-95-237:~/desa/django-tutorial$ 
git status   (estamos en main)
git checkout -b desarrollo
git status   (estamos en desarrollo)
git log --decorate (para ver puntero a rama actual)
creas nuevos fich, modificas fich existentes...
git commit -m -a "xxx" (entregamos cambios en rama desarrollo)
Cuando terminenmos de trabajar en desarrollo. probems q los caambios funcionan, mergeamsos con rama principal
git checkout main (cambiamos a main)
git merge desarrollo
git push -u origin main (local -> remoto)


PRODUCCION
sudo apt update
sudo apt install apache2 uwsgi mariadb-server uwsgi-plugin-python3
....(desa) ubuntu@ip-172-31-95-237:~/desa$ git clone  https://github.com/jbardals/django-tutorial
cd django-tutorial  (aquí repo local de git con el codigo de django)
.....
source bin/activate
pip3 freeze
ubuntu@ip-172-31-95-237:~$ sudo apt install python3-mysqldb
(desa) ubuntu@ip-172-31-95-237:~/desa$ pip3 install mysql-connector-python

6. sudo mysql
create database djangodb;
grant all privileges on djangodb.* to 'djangouser'@'localhost' identified by 'abc123.';
flush privileges;
7. settings.py -> DATABASE
8. (desa) ubuntu@ip-172-31-95-237:~/desa$  python3 manage.py migrate
9. sudo mysql; show databases; use djangodb; show tables;
10. (desa) ubuntu@ip-172-31-95-237:~/desa$ python3 manage.py createsuperuser
11. (desa) ubuntu@ip-172-31-95-237:~/desa$ sudo a2enmod proxy proxy_http
sudo systemctl restart apache2

**Proxy HTTP + uwsgi**
/home/ubuntu/uwsgi.ini
[uwsgi]
http-socket = :8000
#socket = 127.0.0.1:8000
chdir = /home/ubuntu/desa/django-tutorial/
wsgi-file = django_tutorial/wsgi.py
master = true
processes = 4
threads = 2
plugin = python3

#env
home = /home/ubuntu/desa
#vacuum = true

/etc/apache2/sites-available/djang.conf
ServerName django.com
ServerAdmin webmaster@localhost
#DocumentRoot /var/www/django
ProxyPass / http://localhost:8000/

Probar a loguearte en http://django.com/admin con ubuntu/abc123.

**Proxy wsgi**
sudo apt install -y libapache2-mod-proxy-uwsgi
sudo a2enmod proxy_uwsgi
si está habilitado proxy_http hay que deshabilitarlo, pq si no es el q usa por defecto

uwsgi.ini
cambiar http-socket = :8000 -> socket = 127.0.0.1:3032
django.conf
cambiar   ProxyPass / http://127.0.0.1:8000/ -> ProxyPass / uwsgi://127.0.0.1:3032/

**Modulo wsgi de apache**
instalar y activar ```
apt install libapache2-mod-wsgi-py3
sudo a2enmod wsgi
django.conf

ServerName django.com

  ServerAdmin webmaster@localhost
  DocumentRoot /home/ubuntu/desa/django-tutorial
WSGIPassAuthorization On
WSGIDaemonProcess django_tut user=www-data group=www-data processes=1 threads=5 python-path=/home/ubuntu/desa/django-tutorial python-home=/home/ubuntu/desa>
WSGIScriptAlias / /home/ubuntu/desa/django-tutorial/django_tutorial/wsgi.py

<Directory /home/ubuntu/desa/django-tutorial>
        WSGIProcessGroup django_tut
        WSGIApplicationGroup %{GLOBAL}
        Require all granted </Directory

