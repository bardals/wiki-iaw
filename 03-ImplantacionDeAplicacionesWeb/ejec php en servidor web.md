﻿

## Ejecución de php en el contexto web con apache
Habiendo instalado y activado los módulos de PHP 

 - libapache2-mod-php    (modo prefork)
 - php7.2-fpm    (modo event)

los documentos php que queramos ejecutar se suben en el document root (o subdirectorios del document root) de alguno de los virtual host del servidor web. 
P.ej. en XAMPP si subes un fichero test.php_  en el directorio de XAMPP en  **_htdocs_**  (_C:\xampp\htdocs_) se puede acceder al archivo  con la URL _http:// localhost/ test.php

Al introducir el URL _http:// localhost/ test.php_ se está indicando al navegador web que solicite el archivo _test.php_ al servidor web. El servidor Apache HTTP u otro software de servidor web abre el archivo en el directorio correspondiente. La terminación _.php_ informa de que el archivo contiene código PHP. Ahora se pone en marcha el intérprete de PHP integrado en el servidor web, el cual hace un recorrido por el documento hasta dar con la etiqueta PHP de apertura _<?php,_ la cual señala el comienzo del código PHP. Tras ello,el intérprete ya tiene la capacidad de ejecutar el código PHP y de generar una salida en HTML que se envía al navegador desde el servidor web.
