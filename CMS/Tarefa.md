﻿**TAREFA ADAPTACIONS CMS**

 **1 . Creación dun novo tema**

Esta tarefa consiste en escoller un tema, ben dos xa instalados en actividades anteriores ou un novo, e crear un tema fillo a partir del. Haberá que crear os ficheiros e engadirlles o contido necesario para que o novo tema fillo sexa un dos temas dispoñibles en WordPress para poder activalo.

Unha vez que o tema novo estea correctamente creado activarase para que pase a ser o tema de WordPress.

Contesta ademais á seguinte pregunta:

-   Cal é a razón de que non se mostre unha imaxe coa previsualización de como quedará o noso novo tema unha vez activado?

 **2 . Engadir ao blog a icona de páxina (favicon)**

Entrega
- Mostrar cal é o tema pai
- Adxuntar os ficheirs creados para o novo tema
- Mostrar os datos do novo tema activado desde o panel de Administración de WP
- Mostrar os cambios para engadir o favicon
- 
